# Depth Captcha

A simple educational game about squat depth, by OpenPowerlifting.

## Licensing

### Code Licensing

All code is Free/Libre software under the GNU AGPLv3+.<br/>
Please refer to the LICENSE file.

### Image Licensing

Images of squat depth are copyrighted by their original authors.

They are reproduced here under terms of 17 U.S.C. § 107 (Fair Use),</br>
the use thereof being for public education.
