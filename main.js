// vim: set ts=4 sts=4 sw=4 et:
//
// Gameplay logic for Depth Captcha.
//
// Copyright (C) 2019, Sean Stangl <sean.stangl@gmail.com>.
// Released under the terms of the GNU AGPLv3+.

// Generates a random integer between min and max, inclusive.
function random_int(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Data gets put into a global context. This performs (re)initialization.
function new_context() {
    return {
        score: 0, // The current score.
        lives: 3, // The game ends when all lives are lost.

        // How many cells are shown in the captcha grid.
        // The number of cells increases with each combo.
        num_cells: 3,

        // When the player makes a choice we think is incorrect,
        // it gets recorded here. At the end of the game, the incorrect
        // choices are presented for review.
        bad_calls: [],
    };
}

// The context on the `window` object.
var context = undefined;

// Called when the player chooses unwisely.
function take_damage(amount, img_src) {
    window.context.bad_calls.push(img_src);
    window.context.lives = window.context.lives - amount;

    // Check for death.
    if (window.context.lives <= 0) {
        // Update the High Score widget.
        document.getElementById("high-score").innerHTML = String(window.context.score);

        // Hide the playfield and bring back the menu.
        toggle_menu();
        return;
    }

    // Update the lives display.
    document.getElementById("lives").innerHTML = String(window.context.lives);
}

// Called when the player earns points.
function earn_points(amount) {
    window.context.score += amount;
    document.getElementById("score").innerHTML = String(window.context.score);
}

// Handler called when an image was clicked.
function handle_click(e) {
    // Super fragile logic, but won't break for our purposes.
    // Borderline lifts count as bad (and thus clickable).
    if (e.target.src.includes("/good/")) {
        take_damage(1, e.target.src);
    } else {
        earn_points(100);
    }

    // If the player lives on, get a new image.
    if (window.context.lives > 0) {
        e.target.src = pick_image(e.target.src);
    }
}

// Called when the Combo button is clicked.
//
// The Combo button means that the player is asserting that all shown lifts
// are good lifts.
function handle_combo() {
    let grid = document.getElementById("captcha-grid");
    let children = grid.children;

    // Check each node for damage.
    for (let i = 0; i < children.length; ++i) {
        let img = children[i].children[0];

        // For combos, bad lifts cause damage.
        if (img.src.includes("/bad/")) {
            take_damage(1, img.src);

            // Taking damage can end the game, so stop here if necessary.
            if (window.context.lives <= 0) {
                return;
            }
        }
    }

    // If the player lives, increase the grid size and refresh.
    if (window.context.lives > 0) {
        // Award points based on the grid size.
        // Combos are worth a little more.
        earn_points(window.context.num_cells * 150);

        window.context.num_cells = window.context.num_cells + 1;
        new_grid(window.context.num_cells);
    }
}

// Generates a new, empty grid.
function new_grid(num_cells) {
    // Clear the grid.
    let grid = document.getElementById("captcha-grid");
    grid.innerHTML = "";

    // Create however many empty cells.
    for (let i = 0; i < num_cells; ++i) {
        // Create the container div.
        let div = document.createElement("div");
        div.className = "captcha-cell";
        div.id = "cell-" + String(i);

        // Create a child <img> element within that div.
        let img = document.createElement("img");
        img.className = "captcha-img";
        img.src = pick_image("");

        img.addEventListener('click', handle_click);

        div.appendChild(img);
        grid.appendChild(div);
    }
}

// Returns a randomly-chosen image from the pool.
// The "current_image" is the full URL of the image.
function pick_image(current_image) {
    var source;
    let r = Math.random();
    if (r <= 0.1) {
        source = images_borderline;
    } else if (r <= 0.55) {
        source = images_good;
    } else {
        source = images_bad;
    }

    // Take a random image of that type.
    // TODO: This really shouldn't use a loop.
    while (1) {
        let x = random_int(0, source.length - 1);
        if (!current_image.includes(source[x])) {
            return source[x];
        }
    }
}

// Fills in the review <div> on the home screen.
// It shows the images the player got wrong, for learning purposes.
function build_review_pane() {
    let review = document.getElementById("review");
    review.innerHTML = "";

    let bad_calls = window.context.bad_calls;
    if (bad_calls.length === 0) {
        return;
    }

    for (let i = 0; i < bad_calls.length; i++) {
        let div = document.createElement("div");
        let img = document.createElement("img");

        if (bad_calls[i].includes("/good/")) {
            div.className = "captcha-cell review-cell-good";
        } else if (bad_calls[i].includes("/bad/")) {
            div.className = "captcha-cell review-cell-bad";
        }

        img.className = "captcha-img review-img";
        img.src = bad_calls[i];

        div.appendChild(img);
        review.appendChild(div);
    }
}

// Switches between the menu and gameplay.
function toggle_menu() {
    let menu = document.getElementById("home");
    let field = document.getElementById("playfield");

    if (field.style.display === "none") {
        menu.style.display = "none";
        field.style.display = "block";
    } else {
        menu.style.display = "block";
        field.style.display = "none";
        build_review_pane();
    }
}

// Called when clicking the Start button in the menu.
function start_game() {
    // Re-initialize everything.
    window.context = new_context();
    document.getElementById("lives").innerHTML = String(window.context.lives);
    document.getElementById("score").innerHTML = String(window.context.score);

    new_grid(window.context.num_cells);

    // Show the playfield.
    toggle_menu();
}
